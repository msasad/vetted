from django.contrib import admin
from eroll.models import Employee, Company

# Register your models here.
admin.site.register(Employee)
admin.site.register(Company)
