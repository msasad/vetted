from django.contrib.auth.models import User, Group
from django.contrib.auth.hashers import make_password
from eroll.models import Company, Employee, Team, TeamMembers
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups', 'password')
        write_only_fields = ('password',)


class CompanyAdminSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Employee
        fields = ('url', 'username', 'email', 'password')
        extra_kwargs = {
            'password': {'write_only': True},
        }


# class CompanySerializer(serializers.HyperlinkedModelSerializer):
class CompanySerializer(serializers.ModelSerializer):
    admin = CompanyAdminSerializer(many=False)

    def create(self, validated_data):
        print(validated_data)
        password = validated_data['admin'].pop('password')
        admin = Employee.objects.create(**validated_data['admin'])
        admin.set_password(password)
        admin.save()
        company = Company.objects.create(name=validated_data['name'],
                                         admin=admin)
        company.save()
        admin.company = company
        admin.save()
        return company

    class Meta:
        model = Company
        fields = ('url', 'name', 'admin')


class MembershipSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField(source='team.id')
    name = serializers.ReadOnlyField(source='team.name')

    class Meta:
        model = TeamMembers
        fields = ('id', 'name',)


class EmployeeSerializer(serializers.HyperlinkedModelSerializer):
    # applicable only when company admin creates an employee
    def create(self, validated_data):
        print(validated_data)
        # company_data = validated_data.pop('company')
        password = make_password(validated_data.pop('password'))
        employee = Employee.objects.create(password=password, **validated_data)
        return employee

    def update(self, instance, validated_data):
        print(validated_data)
        if 'password' in validated_data:
            instance.set_password(validated_data.pop('password'))
        for attr in validated_data:
            setattr(instance, attr, validated_data.get(attr))

        instance.save()
        return instance


    # company = CompanySerializer(many=False, required=False)
    company = serializers.PrimaryKeyRelatedField(many=False, required=True,
                                                 queryset=Company.objects.all())
    teams = MembershipSerializer(source='memberships', many=True,required=False)

    class Meta:
        model = Employee
        fields = ('url', 'username', 'email', 'password', 'company',
                  'first_name', 'last_name', 'teams')
        extra_kwargs = {
            'password': {'write_only': True},
        }


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')
