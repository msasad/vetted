from django.contrib.auth.models import User, Group
from eroll.models import Company, Employee
from rest_framework import viewsets, permissions
from eroll.serializers import UserSerializer, GroupSerializer, \
    CompanySerializer, EmployeeSerializer


class CreateCompanyPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        print('CreateCompanyPermission', request.method, request.user)
        if request.method not in permissions.SAFE_METHODS:
            if request.user.is_superuser:
                print('CreateCompanyPermission granted')
                return True
            else:
                print('CreateCompanyPermission not granted')
                return False

        print('CreateCompanyPermission not required')
        return True


class CreateEmployeePermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        if request.method == 'POST':
            employee = Employee.objects.get(id=request.user.id)
            if employee.company is None:
                return False
            return request.user.id == employee.company.admin.id
        return True


class UpdateProfilePermission(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS + ('POST',):
            print(self.__class__, 'not required')
            return True
        else:
            if request.method in ('PUT', 'PATCH'):
                if request.user.id == obj.id:
                    print(self.__class__, 'granted')
                    return True
                else:
                    return False
        print('reached here')
        return True


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class EmployeeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = (permissions.IsAuthenticated,
                          CreateEmployeePermission, UpdateProfilePermission)
    queryset = Employee.objects.all().order_by('-date_joined')
    serializer_class = EmployeeSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class CompanyViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    permission_classes = (permissions.IsAuthenticated,
                          CreateCompanyPermission)
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
