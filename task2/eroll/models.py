from django.db import models
from django.contrib.auth.models import User


class Employee(User):
    company = models.ForeignKey(to='Company', null=True, default=None,
                                on_delete=models.DO_NOTHING)
    profile_picture = models.CharField(max_length=128, null=True, default=None)

    # def __init__(self, *args, **kwargs):
    #     super(User, self).__init__(*args, **kwargs)
    #     Token.objects.create(user=self)



class Company(models.Model):
    name = models.CharField(max_length=32)
    admin = models.ForeignKey(to=Employee, on_delete=models.DO_NOTHING,
                              related_name='manages_company', null=True,
                              default=None)

class Team(models.Model):
    name = models.CharField(max_length=32)
    company = models.ForeignKey(to=Company, on_delete=models.DO_NOTHING,
                                related_name='company', null=False)

class TeamMembers(models.Model):
    member = models.ForeignKey(to=Employee, on_delete=models.DO_NOTHING,
                               related_name='memberships', null=False)
    team = models.ForeignKey(to=Team, on_delete=models.DO_NOTHING,
                             related_name='team', null=False)
    class Meta:
        unique_together = (("member", "team"),)
