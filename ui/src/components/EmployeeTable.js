import React from 'react';

class TableRow extends React.Component {
  render() {
    return (
      <tr onClick={e => this.props.showDetails(e, this.props.employee)}>
        <td>{this.props.employee.username}</td>
        <td>{this.props.employee.company}</td>
        <td>{this.props.employee.first_name}</td>
        <td>{this.props.employee.last_name}</td>
      </tr>
    )
  }
}
class EmployeeTable extends React.Component {
  // renderTable(employees) {
  //   return (
  //     {employees.map(employee => this.tableRow(employee))}
  //   )
  // }

  render() {
    return (
      <table>
        <thead>
          <tr>
            <th>Username</th>
            <th>Company</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
        {this.props.employees.map(employee => <TableRow key={employee.username} showDetails={this.props.showDetails} employee={employee}/>)}
        </tbody>
      </table>
    )
  }
}

export default EmployeeTable;
