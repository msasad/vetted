import React from 'react';

class EmployeeDetails extends React.Component {
  render() {
    return (
      <div className="card">
        <h3>
          {this.props.employee.first_name + ' ' + this.props.employee.last_name}
        </h3>
        <div className="teams">
          <ul>
            {this.props.employee.teams.map(team => <li key={team.id}>{team.name}</li>)}
          </ul>
        </div>
      </div>
    )
  }
}

export default EmployeeDetails;
