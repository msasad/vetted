import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import LoginPanel from './LoginPanel';
import EmployeeTable from './components/EmployeeTable';
import EmployeeDetails from './components/EmployeeDetails';

class App extends Component {
  constructor(props) {
    super(props);
    this.showDetails = this.showDetails.bind(this);
    this.state = {
      selected_employee: null,
      employees: [{
        username: 'hulk',
        company: 'marvel',
        first_name: 'Bruce',
        last_name: 'Banner',
        teams: [
          'dev',
          'QA',
          'delivery'
        ]
      },{
        username: 'thor',
        company: 'marvel',
        first_name: 'David',
        last_name: 'Blake',
        teams: [
          'dev',
          'Operations'
        ]
      }, {
        username: 'hawkeye',
        company: 'marvel',
        first_name: 'Clint',
        last_name: 'Barton',
        teams: [
          'dev'
        ]
      }]
    };
  }



  componentDidMount() {
    fetch('http://localhost:8000/employees')
      .then((response,a,b) => {
        console.log(a,b);
        console.log(response);
        if (response.status === 401) {
          window.location.pathname = '/login';
        }
        return response.json();
      })
      .catch(error => console.log(error))
      .then(json => this.setState({employees:json}));
  }
  showDetails(e, employee) {
    console.log(employee);
    this.setState({selected_employee:employee})
  }

  render() {
    return (
      <div>
        <EmployeeTable showDetails={this.showDetails} employees={this.state.employees} />
        { /*{this.state.employees.map(emp => <EmployeeDetails employee={emp} />)} */ }
        {this.state.selected_employee&&<EmployeeDetails employee={this.state.selected_employee} />}
      </div>
    );
  }
}

export default App;
