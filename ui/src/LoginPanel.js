import React, {Component} from 'react';
import './login.css';

class LoginButton extends Component {
  fetchEmployees() {
    fetch('/employees/').then(function(response){
      console.log(response);
      return response.json();
    }).then(function(json) {
      console.log(json);
    });
  }
  onclick(e) {
    console.log(e);
    console.log(this);
    console.log('i was clicked');
  }
  render() {
    return <button className="btn btn-lg btn-primary btn-block" onClick={this.fetchEmployees}>Login</button>
  }
};

class LoginPanel extends Component {
  render() {
    return (
      <form className="form-signin">
        <img className="mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72" />
        <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
        <label htmlFor="username" className="sr-only">Username</label>
        <input type="text" id="username" className="form-control" placeholder="Your username" required autoFocus />
        <label htmlFor="inputPassword" className="sr-only">Password</label>
        <input type="password" id="inputPassword" className="form-control" placeholder="Password" required />
        <div className="checkbox mb-3">
          <label>
            <input type="checkbox" value="remember-me" /> Remember me
          </label>
        </div>
        <LoginButton />
      </form>
    );
  }
}

export default LoginPanel;
