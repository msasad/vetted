def is_prime(number):
    if number in (2,3):
        return True
    if number % 2 == 0 or number % 3 == 0:
        return False

    i = 5
    while i * i <= number:
        if number % i == 0 or number % (i+2) == 0:
            return False
        i += 6
    return True
    # for i in range(2, int(pow(number, 0.5))+1):
    #     if number % i == 0:
    #         return False
    # return True


def prime_generator():
    i = 2
    while True:
        if is_prime(i):
            yield i
        i += 1

def get_nth_prime(n):
    primes = prime_generator()
    for i in range(n):
        prime = next(primes)
    return prime
